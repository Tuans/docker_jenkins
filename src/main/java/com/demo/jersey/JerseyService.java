package com.demo.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/message")
public class JerseyService
{
    @GET
    public String getMsg()
    {
         return "Hello World !! - Jersey 2"
		 + "<h1>This is H1 tag</h1>"
		 + "<h2> This is H2 tag</h2> "
		 + "<h3>This is H1 tag</h3>";
    }
}
